import sqlite3
from flask import Flask, render_template, request, url_for, flash, redirect, send_file
from werkzeug.exceptions import abort

import threading
# from werkzeug import secure_filename
import logging
import threading
import time
import ffmpy
import ffmpeg
import os
app = Flask(__name__)


@app.route('/')
def index():

    return render_template('index.html')

@app.route('/webp')
def webp():

    return render_template('webp.html')

def convert_video_to_gif(f):
    ff = ffmpy.FFmpeg(
         inputs={os.path.join("/home/carmen/video-converter/", f.filename): None},
         outputs={os.path.join("/home/carmen/video-converter/", f.filename.split('.')[0] + ".gif"): None}
    )
    ff.run()
    # clip = (VideoFileClip(f.filename))
    # clip.write_gif("output.gif")

def thread_function(name):
    logging.info("Thread %s: starting", name)
    time.sleep(2)
    logging.info("Thread %s: finishing", name)

@app.route('/uploader', methods = ['GET', 'POST'])
def upload_file():
   if request.method == 'POST':
      f = request.files['file']
      f_name = f.filename.split('.')[0]
    
      f.save(f.filename)
      print(f)
      print("f.filename ", f.filename)

      format = "%(asctime)s: %(message)s"
      logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

      logging.info("Main    : before creating thread")
      x = threading.Thread(target=convert_video_to_gif, args=(f,))
      logging.info("Main    : before running thread")
      x.start()
      logging.info("Main    : wait for the thread to finish")
      x.join()
      logging.info("Main    : all done")
    #   clip = (VideoFileClip(os.path.join("/tmp/", f.filename)))
    #   # print("f.filename ", f.filename)
    #   # print('File ' + f_name + " " + "converted successfully")
 
    #   clip.write_gif("output.gif")

      

      return send_file(os.path.join("/home/carmen/video-converter/", f_name + ".gif"), as_attachment=True)
      # return send_file()


def convert_video_webp(f):
    print(f.filename)
    stream = ffmpeg.input(f.filename).drawtext(text='Giffy', fontsize=54, y=10, x=10, fontcolor='white',box=1, boxcolor='black',boxborderw=5)
    stream = ffmpeg.output(stream, os.path.join("/home/carmen/video-converter/", f.filename.split('.')[0] + ".webp")) 
    ffmpeg.run(stream)

@app.route('/uploadGifToWebm', methods = ['GET', 'POST'])
def upload_video_to_convert_to_webm():
    if request.method == 'POST':
      f = request.files['file']
      f_name = f.filename.split('.')[0]
    
      f.save(f.filename)
      print(f)
      print("f.filename ", f.filename)

      format = "%(asctime)s: %(message)s"
      logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

      logging.info("Main    : before creating thread")
      x = threading.Thread(target=convert_video_webp, args=(f,))
      logging.info("Main    : before running thread")
      x.start()
      logging.info("Main    : wait for the thread to finish")
      x.join()
      logging.info("Main    : all done")
      return send_file(os.path.join("/home/carmen/video-converter/", f_name + ".webp"), as_attachment=True)
    
